#-*-coding: utf-8-*-
def fmaximo(x, y):
	"""3 Función que determina el máximo entre dos números.
4 Args:
x: Argumento de tipo numérico a comparar con el segundo ←-
5
argumento.
y: Argumento de tipo numérico a comparar con el primer ←-
6
argumento.
Returns:
7
La función retorna el mayor de sus argumentos"""
	if x > y:
		return x
	else:
		return y


def test_fmaximo():
#17 Función de prueba de la función fmaximo
	assert fmaximo(3,4) == 4
	assert fmaximo(-4,92) == 92
	assert fmaximo(-10,2) == 2
	assert fmaximo(-5,72) == 72

