def Fib(n):
    a,b = 0,1
    for i in range(n):
        a,b = b, a+b
    return a

for i in range(11):
	print (i,".......",Fib(i))
        


def fact3(n): #iteration
	res=1
	for i in range(2,n+1):
		res=res*i
	return res

def factorial(n): #recursive
	if n==0:
		return 1
	return factorial(n-1)*n
