def cuentaregresiva(n):
	"""Diseño:
				Numero: natural
			signatura:
					nat->none
			proposito:
					Realiza una cuenta regresiva a partir de un numero 
					natural dado, imprimiendo en pantalla un
					mensaje al llegar al valor cero"""
	if n==0:
		print("Achis")
	else:
		print(n)
		cuentaregresiva(n-1)

#-----------------------------------------------------------------------
def factorial_pasos(n):
	"""Diseño:
			Número: 
				Natural.
			Signatura:
				Nat->Nat
			Propósito:
				Calcular el factorial de un número natural dado en forma 
				recursiva, e imprimir los pasos intermedios en el proceso de 
				cómputo a modo de debugging.
		Ejemplos:
			factorial_pasos(5) = 120
			factorial_pasos(0) = 1"""
	print("La función factorial se invocó con n = ", n )
	if n==0:
		return 1
	else:
		resultado = n*factorial_pasos(n-1)
		print("El resultado intermedio para ", n, " * factorial(", n-1, "): ", resultado)
		return resultado
		
#-------------------------------------------------------------ejercicio1
def lucca(n):
	if n==0:
		return 2
	elif n==1:
		return 1
	elif n>=2:
		return lucca(n-1) + lucca(n-2)

#+-+-+-+-+-+-+-+-+-+-+-

def pell(n):
	if n==0:
		return 0
	elif n==1:
		return 1
	elif n>=2:
		return 2 * pell(n-1) + pell(n-2)
		
#------------------------------------------------------------ejercicio2

"""Diseñar una función que calcule e imprima el resultado de la suma de los primeros
50
números naturales usando una función recursiva."""

def suma50():
	n=0
	for i in range(0,51):
		n = (n+i)
	print("La suma de los primeros 50 nat es", n)
	return n
	
#version recursiva
"""def suma50r(n):
	if n==50:
		return n
	else:
		total= n + suma50r(n+1)
	print(total)
	return total"""

def suma50cre(n):
	
	if n!=50:
		total= n + suma50cre(n+1)
		#print(total)
		return total
	else:
		return 0
	
print("La suma de los primers 50 es ",suma50cre(0))

def suma50dec(acum, n):
	if n==0:
		return acum
	else:
		return suma50dec((acum+n), (n-1))

print("La seuma decrec de los prim 50 es", suma50dec(0, 0))
print("La seuma decrec de los prim 50 es", suma50dec(0, 50))



def suma50r(acum, n):
	if n==50:
		
		return (acum+50)
		
		
	else:
		print (acum+n)
		return suma50r(acum+n,n+1)
		
		
suma50r(0,0)

"""
Diseñar una función que calcule e imprima el resultado de la suma de los primeros
n números naturales usando una función recursiva."""

def suman(n):
	tot=0
	for i in range(0,(n+1)):
		tot = tot+i
	print("La suma de los primeros", n ,"nat es", tot)
	return tot
	
def sumanr(n):
	if n!=0:
		total= 0 + suman(n-1)
		print(total)
		return total
	else:
		return n

#print("asdasdadsdsdsdsdsds", sumanr(50))

"""
Diseñar una función que calcule e imprima el resultado de la suma de los números naturales
mayores que n y menores que m usando una función recursiva."""

#mayores que n. menores que m
def sumaymen(n,m):
	if n!=m:
		total = n+ (1 + sumaymen(n+1,m))
		print("JJJJJJJJJJJJJJJJ", total)
		return total
	else:
		return n

#sumaymen(10,15)
