#-*-coding: utf-8-*-
#Representamos temperaturas mediante números
#Representamos temperaturas mediante números enteros
#farCel: Int -> Int
#El parámetro representa una temperatura en Fahrenheit y,
# se retorna su equivalente en Celsius.

"""#Representamos temperaturas mediante números enteros
2 #farCel: Int -> Int
3 #El parámetro representa una temperatura en Fahrenheit y,
4 # se retorna su equivalente en Celsius.
5 # entrada: 32, salida: 0
6 # entrada: 212, salida: 100
7 # entrada: -40, salida: -40"""

def farCel(f):
	return (f-32)*5/9
	

"""#Representamos millas, pies, pulgadas y metros como números
2 #convierteAMetros: Float -> Float -> Float -> Float
3 #El primer parámetro representa una longitud en millas,
4 #el segundo en pies, el tercero en pulgadas y, el valor de
5 #retorno representa su equivalente en metros.
6 #entrada: 1, 0, 0; salida: 1609.344
7 #entrada: 0, 1, 0; salida: 0.3048
8 #entrada: 0, 0, 1; salida: 0.0254
9 #entrada: 1, 1, 1; salida: 1609.6742"""

def convierteAMetros(numeroMillas, numeroPie, numeroPulgadas):
	metros = 1609.34 * numeroMillas + 0.3048 * numeroPie + 0.0254 * numeroPulgadas
	return metros
	
def fmaximo(x, y):
	"""
3 Función que determina el máximo entre dos números.
4 Args:
x: Argumento de tipo numérico a comparar con el segundo ←-
5
argumento.
y: Argumento de tipo numérico a comparar con el primer ←-
6
argumento.
Returns:
7
La función retorna el mayor de sus argumentos"""
	if x > y:
		return x
	else:
		return y

def test_fmaximo():
#17 Función de prueba de la función fmaximo"""
	assert fmaximo(3,4) == 4
	assert fmaximo(-4,92) == 92
	assert fmaximo(-10,2) == 2
	assert fmaximo(-5,72) == 72
