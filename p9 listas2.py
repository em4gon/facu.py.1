"""
Mutabilidad de las listas

Las listas pueden modificarse.
a) append: agregar al final
l = [78455,1]
l.append(77245)

b) insert: agrega un elemento en la posicion indicada, desplazando el 
resto hacia la derecha
l.insert(2,54988)
	l.insert(donde,que cosa)
no se controla la unicidad
"""

#Como hacemos para checquear unicidad?
def agregaUno():
	num = int(input("ingrese valor "))
	k = int(input("ingrese un indice"))
	if num in l:
		print("el valor ya existe")

	else:
		l.insert(k,num)
		
l=[78455,79211,54988,66540,47890,200]

def agregaUno2(lista):
	num = int(input("ingrese valor "))
	k = int(input("ingrese un indice: "))
	contador=0
	longitud=len(lista)
	while contador < longitud and num!=lista[contador]:
		contador+=1
		
	if contador==longitud:
		print("No esta")
		lista=lista[0:k]+[num]+lista[k:len(l)]
		print(lista)
	else:
		print("ya esta", num, "es igual ", lista[contador],"estaba en ", contador)
	

#agregaUno2(l)			
			
def buscaUnoFor(l):
	num = int(input("ingrese valor "))
	k = int(input("ingrese un indice"))
	flag=False
	for elem in l:
		if (num==elem):
			flag=True
			break
	#flag es si lo encontro, o no al elemento, el break no es necesario
	print(l)

#buscaUnoFor(l)

"""
c) elimminar un elemento de una lista
operacion se llama remove
l.remove(¿que cosa?)
solo lo remueve una vez

elimina el elemento indicado, y desplaza el resto de la lista hacia
la izquierda

si hay elementos repetidos solo lo elimina el primero
"""

#como eliminamos todos los repetidos
"""
remove 
in 
usar while"""

#remove_all: list(num)->num-> list(num)
#(iguales) con el elemento especificado e y retoma la lista resultante/modificado

def remove_all(lista,elemento):
	while elemento in lista:
		lista.remove(elemento)
	else:
		return lista
		
lista42=[2,2,2,2,2,1]
print(remove_all([],2))
	
#testing
def test_remove_all():
	listaprueba=[1,2,2,5,7,2]
	assert remove_all(listaprueba,1) == [2,2,5,7,2]
	assert remove_all(listaprueba,2) == [1,5,7]
	assert remove_all([],2) == []#?
	assert remove_all([2,2,2,2,2],2) == []
	assert remove_all(listaprueba,1) == [2,2,5,7,2]


"""
c) buscar dentro de ua lista

1)con iterador for
2) con ciclo while
3) con una funcion python -> elemento -> in
						-> indice/posicion -> index
						
que queremos buscar?
a) elemento
b) posicion de un elemtno

l.index(que quiero buscar)

l.index(78455)

si un elemento esta repetido solo nos devuelve el indice de la primera
aparicion. Si no se encuentra en la lista nos da un mensaje de error

l.index(1000)
<<error>>

ciclo while
l=[.....]
num=int(input("ingrese un valor"))
i=0 #contador indice
longitud=len(l)
while(i<longitud and num!=l[i]):
	i+=1
if(i==longitud):
	print("no esta el elemento")
else:
	print("esta el elemento", num)
"""

"""
indice: numero
index_all; lista(num)->num->lista(num=
retorna una lista con todos los indices donde ocurre el elemento especificado en la lista dada

"""

def index_all(lista,elemento):
	indexe=[]
	contador=0
	for e in lista:
		if elemento == e:
			indexe+=[contador]
		contador+=1	
	print(indexe)

	
	
	

lista992=[5,8,6,2,4,5,2,3,7,5,9]
#index_all(lista992,5)
#index_all([],5)

#testing
def test_index_all():
	lista_testing=[1,5,5,2,11,-1,-5,5]
	assert(index_all(lista_testing,15)==[])
	assert(index_all(lista_testing,11)==[4])
	assert(index_all(lista_testing,5)==[1,2,7])
	assert(index_all([],5)==[])


#con ciclos for
"""
a) elemento
i=0
for elem in l:
	if elem == num:
		break
	i=i+1
print(....
"""

"""	
otra forma................
for i, elem in enumerate(l):
	if elem==num:
		break
print....

b) indice
longitud=len(l)
for i in range(0,longitud):
	if l[i]==num:
		break
print(........
"""

lista22=[1,2,3,4,5]
def index_all2(lista,elemento):
	for i,elem in enumerate(lista):
		if elem==elemento:
			break
	print("indice:",i,"     elemento",elem)		

index_all2(lista22,3)


#parcial     29/5
#testing sobre listas
#iterar sobre listas, while for
#insertar e insertar con unicidad
#eliminar, eliminar con repeticion
