#-*-coding: utf-8-*-

"""
1) funcion de creacion de tuplas
2) testing con tuplas
3) iteracion con tuplas c/ indices elementos

fecha(int,int,int)
	:(dia,mes,año)
año<0 indica fecha anterior al nacimiento de cristo

tiempo:(int,int,int)
	:(hora,min,seg)
	
alumno:(int,stirng,string,fecha)
	:(legajo,nombre,apellido,fecha de ingreso)

crear_fecha: int->int->int->Fecha
							(int,int,int)
crea una entidad/objeti/elemento del tipo fecha
 segun lo declarado en el Diseño de Datos
"""

def crea_fecha(dia,mes,año):
	if (dia>=1 and dia<32) and (mes>=1 and mes<13):
		
		return (dia,mes,año)
	else:
		return (0,0,0)
		
#main: None->None

def pide_datos():
	dia=int(input("Ingrese un dia: "))
	mes=int(input("Ingrese un mes: "))
	año=int(input("Ingrese un año: "))
	return(dia,mes,año)
	
def main():
	#dia=int(input("Ingrese un dia: "))
	#mes=int(input("Ingrese un mes: "))
	#año=int(input("Ingrese un año: "))
	dia,mes,año=pide_datos()
	tfecha=crea_fecha(dia,mes,año)
	while tfecha==(0,0,0):
		if tfecha==(0,0,0):
			print("Los valores ingresados son erroneos")
			tfecha=pide_datos()
	else:
		print("La fcha es ingresada es", tfecha)
			
		
#main()

"""
Tarea
*completar las funciones de creacion para tiempo para tiempo y alumno
*pedir nuevamente la inforacion cuando esta es incorrecta en Fecha. Utilizamos un while true con un break
"""

def test_creaFecha():
	assert crea_fecha(5,5,2018)==(5,5,2018)
	assert crea_fecha(-1,5,2018)==(0,0,0)
	assert crea_fecha(5,-1,2018)==(0,0,0)
	assert crea_fecha(5,5,-200)==(5,5,-200)
	assert crea_fecha(-2,-1,-55)==(0,0,0)
	assert crea_fecha(0,0,0)==(0,0,0)
	

"""
Nota al margen
y con strings??????

str="La casa esta en Portugal"

#ch: char, elemento de la string
#ITERADOR DE ELEMENTOS
for ch in str: #["L","a","C","a","s"....]
	print(ch, end="\t")
print("\n")

#LA ITERACION SE DA POR UN INDICE
longitud=len(str)#[0,1,2,3,4,5,6,7...23]
for i in range(0, longitud):#el iterador es un entero, i: indice
	print(str[i],end="\t")


"""



#Iteracion con tuplas
#1) CON ELEMENTOS

def iteraconConTuplas():
	tupla=(3,1,5,12,20)
	suma=0
	for tup in tupla:
		suma=suma+tup
	prom=suma/len(tupla)
	print("El promedio de la tupla es", prom)
	
iteraconConTuplas()

#2) con idexacion

def conIdexacion():
	tupla=(3,1,5,12,20)
	suma=0
	longitud=len(tupla)
	for i in range(0,longitud):
		suma+=tupla[i]
	prom=suma/longitud
	print("El promedio de la tupla es ", prom)
	
#conIdexacion()

