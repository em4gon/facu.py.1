"""
Como ejemplo del uso de Tuplas vamos a escribir un programa que permita llevar una
Agenda de Contactos. Para esto crearemos funciones que nos permitan:
1. agregar un contacto;
2. buscar un contacto;
3. eliminar un contacto;
4. mostrar los contactos existentes:
Lo primero que vamos a hacer es definir qué estructura va a tener un contacto: vamos a
pensarlo como tupla de 3 elementos: nombre, apellido y edad. Los dos primeros son cadenas
y, el tercero, un número por lo que el tipo de nuestra Tupla sería: (String, String, Int)


1) modificarcontacto para incluir campos direccion y codigo postal
2) ingresar conservando unicidad

contacto=IngresarContacto()
listaDeContactos.append(contacto)

punto2:
listaDeContactos+=[contacto[

unicidad -> si no esta -> agregarlo
		si esta -> el contacto existe y no se lo puede agregar

3)devolver contactos

4) devuelve contactos por direccion
5)devolver el indice donde se eneucntra un contacto dado nombre y apellido
6)eliminar sin recursion
7)completar la receta
8)esta se toma en el parcial (remover contacto)
"""


#ejercicio3
#devuelve una lista con todos los contactos que coincidan con nombre y apellido
def devuelveContactos(listaDeContactos,Nombre,Apellido):
	ln=[]
	for contacto in ListaDeContactos:
		if(contacto[0]==Nombre)and contacto[1]==Apellido:
			ln+=[contacto]
	return ln

def ingreseContacto():
	#none->Contacto
	#contacto debe estar definita previamente
	#para que sea testeable debe recibir los datos en vez de pedirlos con input
    nombre = input("Ingrese el nombre: ")
    apellido = input("Ingrese el apellido: ")
    edad = int(input("Ingrese la edad: "))
    return (nombre, apellido, edad)
 
lc1=[("Sandra","Perez",10),
("Jose","San Martin", 82),
("Juan","Garcia",20)]

print(lc1[1][0])

def buscaContacto(listaDeContactos, nombre, apellido):
    for contacto in listaDeContactos:
        if contacto[0]==nombre and contacto[1]==apellido:
            return True
    return False

def buscaContactoWhile(listaDeContactos, nombre, apellido):
    indice = 0
    longitud = len(listaDeContactos)
    while indice < longitud and (listaDeContactos[indice][0]!= nombre or listaDeContactos[indice][1]!= apellido):
        indice += 1
    return (indice<longitud)

def muestraListaContactos(listaDeContactos):
    for i in range(0, len(listaDeContactos)):
        print("Contacto ",i+1)
        contacto = listaDeContactos[i]
        print("Nombre: ", contacto[0])
        print("Apellido: ", contacto[1])
        print("Edad: ", contacto[2])
 
 
#def muestraListaContactos(listaDeContactos):
#    for i in range(0, len(listaDeContactos)):
#        print("Contacto ",i+1)
#        listaDeContactos[i]
#        print("Nombre: ", listaDeContactos[i][0])
 #       print("Apellido: ", listaDeContactos[i][1])
  #      print("Edad: ", listaDeContactos[i][2])

def eliminaContacto(listaDeContactos, nombre, apellido):
    indice = 0
    longitud = len(listaDeContactos)
    while indice < longitud and (listaDeContactos[indice][0]!= nombre or listaDeContactos[indice][1]!= apellido):
        indice += 1
    if (indice<longitud):
        listaSinContacto = listaDeContactos[0:indice]+listaDeContactos[indice+1:longitud]
        return eliminaContacto(listaSinContacto, nombre, apellido)
    else:
        return listaDeContactos
    
def menu():
    listaDeContactos = []
    print("1-Agregar, 2-Buscar, 3-Eliminar, 4-Mostrar, 5-Salir")
    opcion = int(input("Ingrese una opcion: "))
    while opcion != 5:
        if opcion == 1:
            listaDeContactos += [ingreseContacto()]
        if opcion == 2:
            nombre = input("Ingrese el nombre del contacto a buscar: ")
            apellido = input("Ingrese el apellido del contacto a buscar: ")
            if buscaContacto(listaDeContactos, nombre, apellido):
                print("El contacto esta en la lista")
            else:
                print("El contacto no existe")
        if opcion == 3:
            nombre = input("Ingrese el nombre del contacto a eliminar: ")
            apellido = input("Ingrese el apellido del contacto a eliminar: ")
            listaDeContactos = eliminaContacto(listaDeContactos, nombre, apellido)
        if opcion == 4:
            muestraListaContactos(listaDeContactos)
        print("1-Agregar, 2-Buscar, 3-Eliminar, 4-Mostrar, 5-Salir")
        opcion = int(input("Ingrese una opcion: "))

