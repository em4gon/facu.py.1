import random
import modul
import math

"""
1. Diseñar un programa qu epermite ingresar dos valores numericos y calcular sobre ellos distintas oper
aritmeticas basicas, segun su menu de opciones
estas son: suma resta(2) multiplic(3) divison(4)
Las op podras repetrise indefinidamente hasta que el user desee finalizar el prorama- por lo tanto 
este mosrara en el menu dos opciones mas
0- cargar datos
5-salir

2. mantenimiento y mejoras de 1
a) utilizando el modulo random agregar una opcion mas al menu que cargue dos datos 
aleatorios sobre los cuales aplicar las operaciones anteriores

b)utilizando el modulo math agregar una opcion mas al menu que permita realizar 
calculos complejos sobre los valores ingresados. por ejemplo.
	maximo comun divisor, hipotenusa: gcd, hypot
	funciones trigonometricas: sin con tan
	
c)crear un modulo propioque contenga funciones sobre numeros como:
	factorial (iterativa)
	fibonacci (iterativa)
	maximo
	minimo
y agregar estas operaciones al menu anterior (sub-menu de b)

"""

"""
valores numericos: int
int-> int-> int
calcula la suma de valores dados
"""

def suma(a,b):
	#print (a+b)
	return a+b
	
jota= suma(10,15)

#print(jota+100000)


def test_suma():
	assert suma(0,0)==0
	assert suma(1,0)==1
	assert suma(1,1)==2
	assert suma(0,1)==1
	assert suma(1,-1)==0
	assert suma(-1,1)==0
	assert suma(-1,-1)==-2

"""
valores numericos int
int->int->int
calcula la resta del primer numero por el segundo
"""
def resta(a,b):
	return a-b

def test_resta():
    assert resta(9,5)==4
    assert resta(-5,-5)==-1

def multiplicacion(a,b):
#Valores numericos: int
#int -> int -> int
#calcula la multiplicacion del primer numero por el segundo
        return a*b
"""
#float division comun /
#int division entera //
"""
#division:int->int->int -0(cero)
#calcula la division entera entre dos valores, chequeamos no dividir poir cero

def division(n,m):
    if m!=0:
        return n/m

#def test_division():
	
	
#print(division(-25,5))

"""
menu
None->int
muestra el menu de opciones del programa, recordando la seleccion del user
"""

"""
def menu():
	print("0. Carga de datos")
	print("1. Suma")
	print("2. Resta")
	print("3. Multiplicacion")
	print("4. Division")
	print("5. Salir")
	opcion=0
	opcion1= input("Coloque la opcion ")
	while type(opcion1) != int or type(opcion1) != float:
		opcion=int(opcion1)
		return opcion
	if type(opcion1) == int or type(opcion1) == float:
		opcion=int(opcion1)
		return opcion
	else:
		print("Coloque un numero entre 1 y 5aaaaaaaaaaaaaaaa")
		menu()
		#opcion= input("Coloque la opcion ")
"""
def menu():
	print("0. Carga de datos")
	print("1. Suma")
	print("2. Resta")
	print("3. Multiplicacion")
	print("4. Division")
	print("5. Salir")
	opcion= int(input("Coloque la opcion "))
	return opcion

	
"""
carga-datos: None->int, int
ingreso de datos para el problema
"""

def carga_datos():
	dato1=int(input("Ingrese el dato 1: "))
	dato2=int(input("Ingrese el dato 2: "))
	return dato1,dato2
	


def main():
	opcion=-1
	valor1=0
	valor2=0
	while (opcion!=5):
    #mostrar el menu
    #segun la opcion redirigir a las funciones en cada operacion (pseudocodigo)
		opcion=menu()
		if opcion==0:
			valor1, valor2=carga_datos()
		if opcion==1:
			resultado=suma(valor1, valor2)
			print("La suma de ", valor1, "con ", valor2, "es ", resultado)
		elif opcion==2:
			resultado=resta(valor1,valor2)
			print("La resta de", valor1, "con", valor2, "es", resultado)
		elif opcion==3:
			resultado=multiplicacion(valor1,valor2)
			print("La multiplicacion entre ", valor1, "y ", valor2, "es", resultado)
		elif opcion==4:
			resultado=division(valor1,valor2)
			print("La division entre ", valor1, "y ", valor2, "es ", resultado)
		elif opcion==5:
			print("Fin del programa")
		else:
			print("Opcion incorrecta")
#			else:
#				print("Ingrese un numero entre 1 y 5pija")

#----------------------------------------------------------------------------------------1
#main()
#----------------------------------------------------------------------------------------1
def menu2():
	"""
	None->int
	muestra el menu de opciones del programa, recordando la seleccion del user
	"""
	print("0. Carga de datos")
	print("1. Suma")
	print("2. Resta")
	print("3. Multiplicacion")
	print("4. Division")
	print("5. Asignar valores aleatorios")
	print("6. Operaciones complejas")
	print("7. Salir")
	opcion= int(input("Coloque la opcion "))
	return opcion

def tuplarandom():
	valor1=random.randint(-1000,1000)
	valor2=random.randint(-1000,1000)
	return (valor1,valor2)

def submenu():
	modul.limpiapantalla()
	print("a. Carga de datos")
	print("b. Seno")
	print("c. Coseno")
	print("d. Fibonacci")
	print("e. Volver al menu")
	opcion = input("Coloque la opcion: ")
	return opcion
	
"""
def main2 ():
	opcion=-1
	valor1=0
	valor2=0
	while (opcion<7):
    #mostrar el menu
    #segun la opcion redirigir a las funciones en cada operacion (pseudocodigo)
		opcion=menu2()
		if opcion==0:
			valor1, valor2=carga_datos()
		elif opcion==1:
			resultado=suma(valor1, valor2)
			print("La suma de ", valor1, "con ", valor2, "es ", resultado)
		elif opcion==2:
			resultado=resta(valor1,valor2)
			print("La resta de", valor1, "con", valor2, "es", resultado)
		elif opcion==3:
			resultado=multiplicacion(valor1,valor2)
			print("La multiplicacion entre ", valor1, "y ", valor2, "es", resultado)
		elif opcion==4:
			resultado=division(valor1,valor2)
			print("La division entre ", valor1, "y ", valor2, "es ", resultado)
		elif opcion==5:
			print("Fin del programa")
			break
		elif opcion==6:
			valor1,valor2=tuplarandom()
	
	#mostrar el submenu		
	while opcion>6:
		opcion=submenu()				
		if opcion == 8:
			valor1, valor2=carga_datos()
		elif opcion == 9:
			resultado=(math.sin(valor1),math.sin(valor2))
			print("El seno de ", valor1, "es ", resultado[0], " y el seno de ", valor2, " es ", resultado[1])
		elif opcion == 10:
			resultado=(math.cos(valor1),math.cos(valor2))
			print("El coseno de ", valor1, "es ", resultado[0], " y el coseno de ", valor2, " es ", resultado[1])
		elif opcion ==11:
			resultado=(modul.fib(valor1),modul.fib(valor2))
			print("El n de fibonacci de ", valor1, "es ", resultado[0], " y el de ", valor2, " es ", resultado[1])
		else:
			modul.limpiapantalla()
			opcion=menu()
			
			
	#else:
	#	print("Opcion incorrectaaaaaaaaa")
		#continue
"""

def main2 ():
	opcion=-1
	dentrosubmenu=False
	valor1=0
	valor2=0
	while ((str(opcion).isalpha()) or (opcion<7)) and dentrosubmenu==False:
    #mostrar el menu
    #segun la opcion redirigir a las funciones en cada operacion (pseudocodigo)
		opcion=menu2()
		if opcion==0:
			valor1, valor2=carga_datos()
			modul.limpiapantalla()
		elif opcion==1:
			resultado=suma(valor1, valor2)
			print("La suma de ", valor1, "con ", valor2, "es ", resultado)
			modul.limpiapantalla()
		elif opcion==2:
			resultado=resta(valor1,valor2)
			print("La resta de", valor1, "con", valor2, "es", resultado)
			modul.limpiapantalla()
		elif opcion==3:
			resultado=multiplicacion(valor1,valor2)
			print("La multiplicacion entre ", valor1, "y ", valor2, "es", resultado)
			modul.limpiapantalla()
		elif opcion==4:
			resultado=division(valor1,valor2)
			print("La division entre ", valor1, "y ", valor2, "es ", resultado)
			modul.limpiapantalla()
		elif opcion==5:
			valor1,valor2=tuplarandom()
			print("Se eligieron los valores ", valor1, "y ", valor2)
			modul.limpiapantalla()
			#opcion=submenu()
		elif opcion==7:
			print("Fin del programa")
		elif opcion==6:
			#opcion=submenu()
			dentrosubmenu=True	
			while dentrosubmenu==True:
				opcion=submenu()
				if opcion == "a":
					valor1, valor2=carga_datos()
					modul.limpiapantalla()
				elif opcion == "b":
					resultado=(math.sin(valor1),math.sin(valor2))
					print("El seno de ", valor1, "es ", resultado[0], " y el seno de ", valor2, " es ", resultado[1])
				elif opcion == "c":
					resultado=(math.cos(valor1),math.cos(valor2))
					print("El coseno de ", valor1, "es ", resultado[0], " y el coseno de ", valor2, " es ", resultado[1])
				elif opcion =="d":
					resultado=(modul.fib(valor1),modul.fib(valor2))
					print("El n de fibonacci de ", valor1, "es ", resultado[0], " y el de ", valor2, " es ", resultado[1])
				else:
					modul.limpiapantalla()
					dentrosubmenu=False
					#opcion=	menu2()
					#modul.limpiapantalla()
	
			
	

main2()



#ej3
#print(modul.min(9,4))
