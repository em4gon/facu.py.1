
"""
Listas
*longitud de una lista
*indexacion
*mutabilidad
*busquedas

Secuencias  
	-strings
	-tuplas
	-lista
			
Usaremos las listas para modelar datos compuestos, pero cuya cantidad y
valor varian con el paso de l tiempo

-Notacion : []
-type: ([78455, 1235,0])

-Ej: 
	Lista de numeros
		num=[78455,1235,0]
	Lista de strings
		strins=["Hola","Casa","Pipo"]
	Lista de Bool:
		Bool=[True,False,True]
	Lista de tuplas:
		fechas=[(25,5,1980),(2,4,1982),(9,7,1816)]	
	Lista de Listas:
		Listas=[[1,2,3],[5,7,8],[1],[]]
	Lista vacia
		empty=[]
	Lista unitaria
		unitaria=[1]
	
Las listas son MUTABLES
	- por indice
	- por operaciones:
					- append
					- insert
					- remove
"""

"""
Listas
1) longitud-------------------------------------------------------------
	utilizamos la misma funcion para las strins y las tuplas
	len()

Para lista
	len(lista[0])
	suma=0
	for i in range(0,len(lista)):
		suma+=len(lista[i])
	return suma
	
	
	
para los elementos
listaejemplo=[[1,2,3],[5,7,8],[1],[]]
len(lista[0])
suma=0
	for lista in listaejemplo:
	suma=suma+len(lista)
	
	
2) indexacion-----------------------------------------------------------
igual que los strins y las tuplas accedemos a los elementos a travez de
un indice el cual varia entre 0 y la longitud de la lista -1

   0   1   2  3  4   5      -> longitud de la lista -1
[7825,255, 0, 3, 2, 100]    -> lista2
  -6   -5 -4 -2 -2  -1      -> longitud de la lista


3) slices y segmentos---------------------------------------------------
igual que en las strings y en las tuplas

lista2[0:4] el cuatro no lo incluye -> [7825,255,0,3]
==
lista2[:4]


lista2[4:len(listaejemplo)] -------->[2,1000]
==
lista2[4:]

lista2=[0:4]+[4:len(lista2)]
"""

#SLICE
print("Quiero AGREGAR a la lista el valor -1 en el indice 4")
lista2=[7825,255, 0, 3, 2, 100]
print(lista2)
lista2=lista2[0:4]+[-1]+lista2[4:len(lista2)]
print(lista2)

print("Quiero ACTUALIZAR a la lista el valor -1 en el indice 4")
lista2=[7825,255, 0, 3, 2, 100]
print(lista2)
lista2=lista2[0:4]+[-1]+lista2[5:len(lista2)]
print(lista2)


"""
4) MUTABILIDAD----------------------------------------------------------
"""
print("Mutando la posicion 4")
lista2=[7825,255, 0, 3, 2, 100]
print(lista2)
lista2[4]=-1
print(lista2)

"""b
Agregar al final de la lista
lista2=lista2+[25]

oparacion append
"""
lista2.append(-25)
print(lista2)

"""
c) insertar un valor en un indice k y desplazar al resto hacia la 
derecha

operacion insert(indice, elemento)

No garantizaa unicidad en la insercion
"""

lista2.insert(4,-9999)
print(lista2)

# 0 in l -> true or false, verifica si el elemento esta en la lista
