
"""
Cadenas/strings

* Como acceder a una posicion determinada de una cadena
"""
#nombre="Veronica"

"""
01234567
Veronica
0=<rango valido para i <ultimo caracter len-1
len(nombre)==8

Las proposiciones de la cadena se llaman indices

nombre[0]
nombre[8] es invalido
nombre[7] ultimo caracter de veronica

si el numero es negativo empieza a contar desde el otro lado
nombre[-8] primer caracter
nombre[-9] put of range
nombre[-1] ultimo caracter
"""

#for char in nombre:
#	print(char,end="\t")
	
def ejem1():
	#contador=0
	i=0
	lon=len(nombre)
	while i<lon:
		print (nombre[i],end="\t")
		i+=1
	
#ejem1()

"""
Escribir un ciclo que permita mostrar una cadena ingresada por el usuario en su sentido directo y en su sentido inverso
ej: hola
	aloh
	2) alohhola
"""



def ej1():
	nombre=input("Ingrese el nombre a mostrar ")
	i=0
	print(nombre)
	while i > -(len(nombre)):
		i-=1
		print(nombre[i],end="")

#ej1()

print("                                  ")

def ej2():
	nombre=input("Ingrese el nombre a mostrar ")
	i=0
	while i > -(len(nombre)):
		i-=1
		print(nombre[i],end="")
	print(nombre)
	
		
#ej2()

"""
segmentos/slices de cadenas

Veronica -> Verno (es una substring)
		-> roni
		-> ca
		-> eron
		etc
		
substring: tiene que respetar el orden, se puede checkear con la funcion in

>>>nombre[0:2]
"Ve"

str[start:end]
se correspondecon la substring de str que comienza en la posicion/indice start
y termina en la posicion/indice end-1

"""


def separadorpalabras(string):
	string+=" "
	listamostrar=[]
	mare=0
	mars=0
	for i in range(len(string)):
		if string[i]==" ":
			mare=i
			listamostrar+=[string[mars:mare]]
		else:
			
			mars=mare
	return listamostrar
