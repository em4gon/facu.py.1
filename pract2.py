#-----------------ejercicio4

def repite_hola4(n):
	cadena=""
	for i in range(0,n):
		cadena+="Hola"
	return cadena


"""
cadena=cadena+"Hola"
===
cadena+="Hola" """

def repite_hola41(n):
 return n*"Hola"
  
#print(repite_hola41(5))

#= asignacion
#== boolean

#-----------------ejercicio7a
def es_par(n):
	if n%2==0:
		print(n, " es par")
		return True
	else: 
		print(n, " no es par")
		return False
	
def par2(n):
	return n%2==0

#determinar si un numero entero es primo o no
"""
n es mayor o igual a 2
solo divisible por si mismo y por uno"""

def primo(n):
	if n>=2:
		for i in range(2,n):
			if (n%i)==0:
				print(n, " no es primo")
				return False
		print(n, " es primo")
		return True
	else:
		print("El numero debe ser un entero mayor o igual a 2")
		return False

#primo(4)

#dado un número entero n, indicar si es múltiplo de tres o no.
def mult3(n):
	if n%3==0:
		print("Es multiplo de 3")
		return True
	else: 
		print("No es un multiplo de 3")
		return False
		
#mult3(9)

"""8
Escribir una implementación propia de la función abs, que devuelva 
el valor absoluto de cualquier valor que reciba"""

def abs1(n):
	if n>=0:
		return n
	else:
		return n-2*n

#----------------------------------------------------------------------- ejercicio8enclase
"""ejemplo de la diferencia entre 2 ifs y un if+else"""

def pn1():
    x=int(input("ingrese un nro:" ))
    if x>0:
        print("Nro positivo")
        x=-x
    if x<0:
        print("Nro no positivo")
    print("Debugging - x", x)

#pn1()

def pn2():
    x=int(input("Ingrese un nro: "))
    if x>0:
        print("nor2 positivo")
        x=-x
    else:
        print("Nor2 no positivo")
    print("Debugging - x", x)

#pn2()

#------------------------------------------------------------------------ejercicio9
def raizzz(x):
	return x**(1/2)
	
#print("raiz cuadrada de 25 es ", sqr(25))

def raices_reales():
	a= int(input("Coloque el valor a "))
	b= int(input("Coloque el valor b "))
	c= int(input("Coloque el valor c "))
	x= (b**2 - 4*a*c)
	if x>0:
		r = raizzz(x)
		print("Las raices del polinomio son ", ((-b + r)/(2*a)), "y", ((-b - r)/(2*a)) )
		
#raices_reales()

#-----------------------------------------------------------------------ejercicio 9 en clase
"""diseñar una funcion que clasifique un numero entero de la siguiente forma:
si es positivo, escriba "nro positivo", si el nro es igual a cero, "igual a cero",
si el nro es negativo escriba "nro negativo"
"""

#a) if-encadenados/anidados
 
def pos_o_neg():
    x=int(input("ingrese nro:"))
    if x>0:
        print("nro pos")
    else: 
        if (x==0):
            print("igual a cero")
        else:
            print("nro es negativo")
            
#b) decision multiple 

def pos_o_neg2():
    x=int(input("ingrese nro"))
    if x>0:
        print("nro positivo")
    elif x==0:
        print("nro igual a cero")
    else:
        print("nro negativo")


#-----------------------------------indicar si un año es bisiesto 10-a
def bisiesto(y):
	if y%4==0 and (y%400==0 or not y%100==0):
		print("El año es bisiesto")
		return True
	else: 
		print("El año no es bisiesto")
		return False

#y%4==0 and (y % 100 != 0 or (y % 100==0 and % 400 == 0))

#print(bisiesto(2016))

#probar este........... devuelve
def bisiest2(a):
	return a%4==0 and	(a%100!=0 or (a%100==0 and a%400==0))
	

#------------------------------------ 10-b
"""Dado un mes, devolver la cantidad de días correspondientes"""
def cant_dias():
    m= input("Coloque el numero o nombre del mes ")
    b = bisiesto(int(input("¿Que año es?")))
    if m=="abril" or m=="4" or m=="junio" or m=="6" or m=="septiembre" or m=="9" or m=="noviembre" or m=="11":
        return 30
    elif m=="febrero" or m=="2":
        if bisiesto == True:
            return 29
        else:
            return 28
    else:
        return 31

#print(cant_dias())

#-----------------------------------------------------------------ejemplo de recursion, factorial
def fac_D(n):
    print ("DEBG-- llamado con n", n)
    if n==0:
        return 1
    else:
        res=n*fac_D(n-1)
        print("DEBG, intermedio", n, "factorial(", n-1, "):", res)
        return res

#fac_D(7)

#------------------fibonacci1 ejemplo
def fib(n):
    if (n==0) or (n==1):
        return 1
    else :
        return fib(n-1)+fib(n-2)

#---------------fibonacci2 ej
def fib(n):
    if (n==0):
        return 0
    elif (n==1):
        return 1
    else:
        return fib(n-1)+fib(n-2)

#----------------------------------------------------ejercicio 14
#argumento: algo que recibe la funcion
def pares25a(a):
	for i in range(0,a):
		print(i*2)
		
#---------------------------------------------------------ejercicio 16
	
def pares25c(a,b):
	if b%2==0:
		par=b+2
	else:
		par=b+1
	for i in range(0,a):
		print(par)
		par+=2 #=par=par+2
	

#----------------------------------------------------------ejercicio19
def diaa(a):
	if a>=1 and a<=366:
		n=a%7
		if n==1:
			return "Lunes"
		elif n==2:
			return "Martes"
		elif n==3:
			return "Miercoles"
		elif n==4:
			return "jueves"
		elif n==5:
			return "viernes"
		elif n==6:
			return "sabado"
		else:
			return "domingo"
	else:
		return "error"


#------------------------------------------------------ejercicio20
"""Programa de astrología: el usuario debe ingresar el día y mes de su cumpleaños
y el programa le debe decir a qué signo corresponde."""

def astrologia(dia,mes):
	if dia<=31 and dia>=1 and mes<13 and mes>0:
		if (dia<20 and mes==1) or (dia>21 and mes==12):
			return "capricornio"
		if (dia>21 and mes==1) or (dia<20 and mes==2):
			return "acuario"
	else:
		return "errrrrrrror"
		
#--------------------------------------------------------ejercicio21
		
			

